# Glastonbury Checklist

**TICKET**

## Camping
* [ ]  Tent
* [ ]  Sleeping mat
* [ ]  Sleeping bag
* [ ]  Pillow

## Toiletries
* [ ]  Toothbrush
* [ ]  Toothpase
* [ ]  Facewipes
* [ ]  Toilet roll
* [ ]  Anti-bacterial lotion
* [ ]  Sun tan lotion

## For the tent
* [ ]  Head torch
* [ ]  Water bottle
* [ ]  Small bin bags
* [ ]  Small backpack
* [ ]  Battery chargers
* [ ]  Cables


## Food


## Clothes
* [ ]  Wellies
* [ ]  Walking shoes
* [ ]  Warm floppy hat
* [ ]  2 Jeans
* [ ]  5 t-shirts
* [ ]  5 pants
* [ ]  5 normal socks
* [ ]  3 warm socks
* [ ]  Waterproof trousers
* [ ]  Anorak



